const possibleValues = ['BLOCK', 'SHOOT', 'EVADE', 'RELOAD']

makeMove = (lastMove) => {
	console.log('Opponents last move was: ');
	console.log(lastMove);
	return {move: possibleValues[Math.floor(Math.random() * possibleValues.length)]}
};


module.exports = {
	makeMove: makeMove
};